<?php

require_once("./classes/misc.class.php");
// We need to use our $db variable (for mysqli) into the class

$GLOBALS = array(
    'db' => $db
);

class Doctor extends Misc {
    protected $glob;

    public function __construct() {
        $this->getDB();
    }

    public function getAll()
    {
    	$allDoctors = array();
        $result = $this->_db->simpleSelect("doctors D", "D.*");
        while ($row = $result->fetch_assoc()) {
        	$doctorsData = array(
			"registration" => $row["registration"],
			"name" => $row["name"],
			"category" => $row["category"],
			"phone" => $row["phone"],
			"mail" => $row["mail"]
        	);
        	array_push($allDoctors, $doctorsData);
    	}
    	return $allDoctors;
    }

    public function getData($registration)
    {
    	if($this->isExist($registration))
    	{
    		$result = $this->_db->simpleSelect("doctors D", "D.*", array("registration", "=", $registration));
    		if($row = $result->fetch_assoc()){
	        	$doctorsData = array(
					"registration" => $row["registration"],
					"name" => $row["name"],
					"category" => $row["category"],
					"phone" => $row["phone"],
					"mail" => $row["mail"]

	        	);
        	}
        	return $doctorsData;
    	}else{
    		return false;
    	}
    }
    public function isExist($registration)
    {
    	// print_r($this->_db);
		$result = $this->_db->simpleSelect("doctors D", "D.registration", array("registration", "=", $registration));
		return $this->_db->haveRows($result);
		
    }
    public function next($registration)
    {
    	// print_r($this->_db);
		$result = $this->_db->advancedSelect("doctors D", array("D.registration"), array(array("registration", ">", $registration." LIMIT 0,1")));
        if($row = $result->fetch_assoc()){
            return $row['registration'];
        }
    }
    public function getName($registration)
    {
    	// print_r($this->_db);
		$result = $this->_db->simpleSelect("doctors D", "D.name", array("registration", "=", $registration));
        if($row = $result->fetch_assoc()){
            return $row['name'];
        }
    }
    public function deleteDoctor($registration)
    {
		$where_array = array("registration", "=", $registration);
		if($this->_db->deleteToDB("doctors", $where_array)){
			// Doctor deleted sucefully ! 
			// ELIMINADO_SATISFACTORIAMENTE
			return "SUCCESS";
		}else{
			// Error
			return "ERROR";
		}
    }
    public function editDoctor($registration, $dataDoctor)
    {
 		$registration = intval($registration);
		
		$array_values = array(
			"name" => $dataDoctor["name"],
			"category" => $dataDoctor["category"],
			"phone" => $dataDoctor["phone"],
			"mail" => $dataDoctor["mail"]
			);

		$where_array = array("registration", "=", $registration);
		if($this->_db->updateToDB("doctors", $array_values, $where_array)){
			// Doctor update sucefully ! 
			// EDITADO_SATISFACTORIAMENTE
			return "SUCCESS";
		}else{
			// ERROR
			return "ERROR";
		}
    }

    public function newDoctor($dataDoctor)
    {

				
		$array_values = array(
			"registration" => $dataDoctor["registration"],
			"name" => $dataDoctor["name"],
			"category" => $dataDoctor["category"],
			"phone" => $dataDoctor["phone"],
			"mail" => $dataDoctor["mail"]
			);


		if(is_int($this->_db->insertToDB("doctors", $array_values))){
			// Doctor created sucefully ! 
			// AGREGADO_SATISFACTORIAMENTE
			return "SUCCESS";
		}else{
			// Error

			return "ERROR";
		}
    }
}
