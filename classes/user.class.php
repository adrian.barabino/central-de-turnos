<?php

// Desde repositorio anterior de Github
// https://raw.githubusercontent.com/adrianbarabino/Polls-System/master/classes/user.class.php
// Creditos: Adrian Barabino


require_once("./classes/misc.class.php");
// We need to use our $db variable (for mysqli) into the class

$GLOBALS = array(
    'db' => $db
);

class User extends Misc {

    protected $glob;

    public function __construct() {
        $this->getDB();
    }



    private function checkPwd($user, $pwd)
    {
        $fields_array = array("U.username", "U.password");
        $where_array = array(array("U.username", " = ", "'".$user."'"), array("U.password", " = ", "'".$this->hashPwd($pwd)."'"));
        $join_array = NULL; 
        $result = $this->_db->advancedSelect("users U",$fields_array,$where_array, $join_array);
        return $this->_db->haveRows($result);
    }

    private function checkUsername($user)
    {

        $result = $this->_db->simpleSelect("users", "username", array("username", "=", $user));

		return $this->_db->haveRows($result);
    }



    public function hashPwd($pwd)
    {
    	$newPassword = md5(sha1($pwd."9iu".crc32($pwd))."10u3jhkl");
    	return $newPassword;
    }

    public function getCurrentUser()
    {
    	if(isset($_COOKIE['userLogged'])){
    		$user_array = json_decode(urldecode($_COOKIE['userLogged']));
    		if(isset($user_array->id)){
    			return $user_array->id;
    		}
    	}else{
                return false;
		} 	
    }
    public function getUserData($userid)
    {

		$sql = sprintf("SELECT U.* FROM users U
		WHERE U.id = '%s' ", $userid);
		$result = $this->glob['db']->query($sql); 
        if($row = $result->fetch_assoc()){
    	$userData = array(
    		"id" => $row['id'],
            "username" => $row['username'],
    		"realname" => $row['realname'],
    		"rank" => $row['rank'],
    		"last_ip" => $row['last_ip'],
    		"password" => $row['password'],
    		);
        	return $userData;
        }else{
        	return $this->glob['db']->error;
        }
    }
    
    public function isAdmin($userid = NULL)
    {
        if($userid == NULL)
            $userid = $this->getCurrentUser();

        if($this->getRank($userid) > 0){
            return true;
        }else{
    		return false;
    	}
    }


    private function getRank($userid = NULL){
        if($userid == NULL)
            $userid = $this->getCurrentUser();

        $result = $this->_db->simpleSelect("users", "rank", array("id", "=", $userid));
        if($row = $result->fetch_assoc()){
            return $row['rank'];
        }else{
            die("User doesn't exist!");
        }       
    }
    public function getRealname($userid = NULL){
        if($userid == NULL)
            $userid = $this->getCurrentUser();

		$result = $this->_db->simpleSelect("users", "realname", array("id", "=", $userid));
        if($row = $result->fetch_assoc()){
        	return $row['realname'];
        }else{
        	die("User doesn't exist!");
        }    	
    }
    private function getUserId($username)
    {
        $result = $this->_db->simpleSelect("users", "id", array("username", "=", $username));
        if($row = $result->fetch_assoc()){
        	return $row['id'];
        }else{
        	die("User doesn't exist!");
        }
    }

    public function isLogged()
    {
        if(isset($_COOKIE['userLogged'])){
            $user_array = json_decode(urldecode($_COOKIE['userLogged']));
            if(isset($user_array->username)){
                return true;
            }
        }else{
            return false;
        }
    }
    public function salir()
    {
    	if(isset($_COOKIE['userLogged'])){
    		$user_array = json_decode(urldecode($_COOKIE['userLogged']));
    		if(isset($user_array->username)){
    			return true;
    		}
    	}else{
    		return false;
    	}
    }
    public function isExist($id)
    {
        // print_r($this->_db);
        $result = $this->_db->simpleSelect("users U", "U.id", array("id", "=", $id));
        return $this->_db->haveRows($result);
        
    }
    public function login($user, $pwd)
    {
    	if(!$this->isLogged()){

	    	if($this->checkPwd($user, $pwd)){

		    	$login_array = array(
		    		"id" => $this->getUserId($user),
                    "username" => $user,
		    		"realname" => $this->getRealname($this->getUserId($user)) ,
		    		"pwd" => $this->hashPwd($pwd),
		    		"rank" => $this->getRank($this->getUserId($user)) 
		    	);

		    	$login_array = urlencode(json_encode($login_array));
		    	setcookie("userLogged", $login_array, time()+72000);
                // INGRESADO_SATISFACTORIAMENTE

    			return "SUCCESS"; 
	    		
	    	}else{
	    		return "WRONG";
	    	}
    	}else{
    		return "ALREADY";
    	}
    }

    public function editUser($id, $username, $realname, $pwd, $rank)
    {
        # code...
            // EDITADO_SATISFACTORIAMENTE

    }
    public function getAll()
    {
        $allValores = array();
        $result = $this->_db->simpleSelect("users U", "U.*");
        while ($row = $result->fetch_assoc()) {
            $valoresData = array(
                    "id" => $row["id"],
                    "username" => $row["username"],
                    "realname" => $row["realname"],
                    "password" => $row["password"],
                    "rank" => $row["rank"],
                    "last_ip" => $row["last_ip"]
            );
            array_push($allValores, $valoresData);
        }
        return $allValores;
    }

    public function getData($id)
    {
        if($this->isExist($id))
        {
            $result = $this->_db->simpleSelect("users U", "U.*", array("id", "=", $id));
            if($row = $result->fetch_assoc()){
                $valoresData = array(
                    "id" => $row["id"],
                    "username" => $row["username"],
                    "realname" => $row["realname"],
                    "password" => $row["password"],
                    "rank" => $row["rank"],
                    "last_ip" => $row["last_ip"]

                );
            }
            return $valoresData;
        }else{
            return false;
        }
    }
    public function register($username, $pwd, $realname, $rank = 0)
    {

	    		if(!$this->checkUsername($username)){
					$array_values = array(
						"username" => $username,
                        "rank" => $rank,
						"realname" => $realname,
						"password" => $this->hashPwd($pwd),
						"last_ip" => $_SERVER['REMOTE_ADDR'],
					);

            if(is_int(($idnuevo = $this->_db->insertToDB("users", $array_values)))){
                        // Usuario created sucefully ! 
                        // AGREGADO_SATISFACTORIAMENTE

                        // return "SUCCESS";
                        print_r($idnuevo);
					}else{
						return false;
					}


	    		}else{
	    			return "Username already used!";
	    		}
    	# code...
    }

    public function logout()
    {
        // SALIDO_SATISFACTORIAMENTE
    	setcookie("userLogged", NULL, time()-3600,  '/', $config['domain']);
        return;
    }
}
