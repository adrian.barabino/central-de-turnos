<?php


require_once("./classes/misc.class.php");

class Validate extends Misc {

    public function __construct() {

    }

    // This was created for a polls system, now i need to migrate it for this proyect. 
	protected function checkByIP($id_poll, $ip)
	{
			
		$fields_array = array("V.ip as 'ip'", "V.id_option", "O.id", "O.id_poll", "P.id as 'idPoll'");
		$join_array = array(
			array("INNER", "options O", "P.id", "=", "O.id_poll"),
			array("LEFT", "votes V", "O.id", "=", "V.id_option")
			);
		$where_array = array(
			array("P.id", "=", $id_poll),
			array("ip", "=", $ip),
			);
		$result = $this->_db->advancedSelect("polls P", $fields_array, $where_array, $join_array);
        return $this->_db->haveRows($result);
	}



	public function validateMail($mail)
	{
		if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			return true;
		}else{
			return false;
		}

	}
}