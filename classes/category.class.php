<?php

// Desde repositorio anterior de Github
// https://raw.githubusercontent.com/adrianbarabino/Polls-System/master/classes/option.class.php
// Creditos: Adrian Barabino

require_once("./classes/misc.class.php");
// We need to use our $db variable (for mysqli) into the class

$GLOBALS = array(
    'db' => $db
);

class Category extends Misc {
    protected $glob;

    public function __construct() {
        $this->getDB();
    }

    public function getAll()
    {
    	$allCategories = array();
        $result = $this->_db->simpleSelect("categories C ORDER BY C.id", "C.*");
        while ($row = $result->fetch_assoc()) {
        	$categoriesData = array(
					"id" => $row["id"],
					"name" => $row["name"],
					"description" => $row["description"]
        	);
        	array_push($allCategories, $categoriesData);
    	}
    	return $allCategories;
    }

    public function getData($id)
    {
    	if($this->isExist($id))
    	{
    		$result = $this->_db->simpleSelect("categories C", "C.*", array("id", "=", $id));
    		if($row = $result->fetch_assoc()){
	        	$categoriesData = array(
					"id" => $row["id"],
					"name" => $row["name"],
					"description" => $row["description"]

	        	);
        	}
        	return $categoriesData;
    	}else{
    		return false;
    	}
    }


    public function isExist($id_category)
    {
    	// print_r($this->_db);
		$result = $this->_db->simpleSelect("categories C", "C.id", array("id", "=", $id_category));
		return $this->_db->haveRows($result);
		
    }
    public function deleteCategory($id_category)
    {
		$where_array = array("id", "=", $id_category);
		if($this->_db->deleteToDB("categories", $where_array)){
			// Category deleted sucefully ! 
		}else{
			// Error
			die("Error deleting the category!");
		}
    }
    public function editCategory($id_category, $name, $description)
    {
		
		$array_values = array(
			"name" => $name,
			"description" => $description
			);
		$where_array = array("id", "=", $id_category);
		if($this->_db->updateToDB("categories", $array_values, $where_array)){
			// Category update sucefully ! 
		}else{
			// Error
			die("Error updating the category!");
		}
    }

    public function newCategory($name, $description)
    {

		$array_values = array(
			"name" => $name,
			"description" => $description
			);

		if($id_category = $this->_db->insertToDB("categories", $array_values)){
			// Category created sucefully ! 
		}else{
			// Error
			die("Error creating the category!");
		}
    }
}