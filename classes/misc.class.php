<?php

// Desde repositorio anterior de Github
// https://raw.githubusercontent.com/adrianbarabino/Polls-System/master/classes/misc.class.php
// Creditos: Adrian Barabino

// require_once("./classes/validate.class.php");
require_once("./classes/db.class.php");
// We need to use our $db variable (for mysqli) into the class

$GLOBALS = array(
    'db' => $db
);

class Misc {
	protected $_db = null;
 
	public function setDB(Db $db) {
		return $this->_db = $db;
	}
 
	public function getDB() {
		if(null == $this->_db) {
			$this->setDB(new Db());

		}
		return $this->_db;
	}

    protected $glob;

    public function __construct() {
    	$this->getDB();
        global $GLOBALS;
        $this->_db =& $GLOBALS;
    }

	public function cleanString($string)
	{
		$string = str_replace("<","<",$string);
		$string = str_replace(">",">",$string);
		$string = str_replace("\'","'",$string);
		$string = str_replace('\"',"\"",$string);
		return $string;
	}


}