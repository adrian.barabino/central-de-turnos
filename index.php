<?php

header('Content-Type: text/html; charset=UTF-8');
error_reporting(E_ALL);
// ######################
// Central de Turnos
// Autor: Adrian Barabino
// Fecha de inicio: Enero del 2018
// ######################

// Cargamos la configuración
require("./data/config.php");
require("./data/connection.php");


// Esto lo tengo que pasar a MISC...
// function alerta($tipo, $valor)
// {
// 	print('<div class="alert alert-'.$tipo.'" role="alert">
// 		  '.$valor.'
// 		</div>');
// }

// Cargamos la página actual

if($_GET['pagina']){

	$page = strtolower($_GET['pagina']);

}else{

	$page = "inicio";

}


if(in_array($page, $config["valid_pages"])){

}else{$page = "404";	

}


// Según nuestra configuración cargamos la plantilla
if(isset($config['theme'])){
}else{
	
	$config['theme'] = "2018";
}



// Se llama al archivo de la estructura de la plantilla

	require("./themes/".$config['theme']."/structure.php");


?>