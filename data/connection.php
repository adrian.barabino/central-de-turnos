<?php


$db = new mysqli($config['db_host'],$config['db_user'], $config['db_pass'], $config['db_name']);

// We connect to our database, and then if we get an error in the connection, we print the error, else, we don't do anything.

if($db->connect_errno){

    printf("Error al intentar conectarse a la base de datos: %s\n", $db->connect_error);
    exit();

}else{
        // We are connected ! 

		/* change character set to utf8 */
		if (!$db->set_charset("utf8")) {
		    printf("Error loading character set utf8: %s\n", $db->error);
		    exit();
		} else {
		    // printf("Current character set: %s\n", $db->character_set_name());
		}

}

?>