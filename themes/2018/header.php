<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $config["web_title"]; ?></title>

	<!-- Cargamos Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/themes/<?php echo $config['theme']; ?>/css/typeaheadjs.css">
    <link rel="stylesheet" href="/themes/<?php echo $config['theme']; ?>/css/style.css">
	  <!-- <link rel="stylesheet" href="/themes/<?php echo $config['theme']; ?>/css/jquery-ui.css"> -->
</head>

  <body class="text-center">
