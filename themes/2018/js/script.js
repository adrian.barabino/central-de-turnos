$(document).ready(function() {
	// Here comes the sun...



$("input.typeahead").typeahead({
	onSelect: function(item) {
		console.log(item);
	},
	ajax: {
		url: "/api.php?action=getCategories",
		timeout: 500,
		displayField: "name",
		triggerLength: 1,
		method: "get",
		loadingClass: "loading",
		preDispatch: function (query) {
			$(this).addClass('loading');
			console.log(query);
			return {
				search: query
			}
		},
		preProcess: function (data) {
			names = [];
			for(var k in data) {
				names.push(data[k].name);
			}
			console.log(names);
			if (data.success === false) {
				// Hide the list, there was some error
				return false;
			}
			// We good!
			$(this).removeClass('loading');
			return names;
		}
	}
});

})