
      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Sitio desarrollado por Adrian Barabino</p>
        </div>
      </footer>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="/themes/<?php echo $config['theme']; ?>/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="/themes/<?php echo $config['theme']; ?>/js/bootstrap-typeahead.min.js"></script>
    <script src="/themes/<?php echo $config['theme']; ?>/js/jquery.doneTyping.js"></script>
    <script src="/themes/<?php echo $config['theme']; ?>/js/jquery.floatThead.js"></script>
    <script src="/themes/<?php echo $config['theme']; ?>/js/jquery.validate.min.js"></script>
    <script src="/themes/<?php echo $config['theme']; ?>/js/bootstrap-add-clear.min.js"></script>
    <!-- <script src="/themes/<?php echo $config['theme']; ?>/js/jquery-ui.min.js"></script> -->
    <script src="/themes/<?php echo $config['theme']; ?>/js/loadingoverlay.min.js"></script>
    <script src="/themes/<?php echo $config['theme']; ?>/js/script.js"></script>
    </script>

	
</body>
</html>
