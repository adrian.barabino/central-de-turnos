<?php
header('Content-type:text/html; charset=utf-8'); 
error_reporting(E_ALL);
ini_set('display_errors', '1');

// Init
require("./data/config.php");
require("./data/connection.php");
require("./classes/category.class.php");
$category = new Category();


$action = $_REQUEST['action'];
if ($action) {
    switch ($action) {
        case 'getCategories':
            print_r(json_encode($category->getAll()));
            break;
        
        case 'getCategory':
            $id = $_GET['id'];
            if ($id)
                print_r(json_encode($category->getData($id)));
            break;
        
        case 'existCategory':
            $id = $_GET['id'];
            if ($id)
                print_r(json_encode($category->isExist($id)));
            break;
        
        case 'editCategory':
            $dataCategory = $_POST;
            $id           = $dataCategory['id'];
            if ($category->editCategory($id, $dataCategory) == "SUCCESS") {
                printf(1);
            } else {
                printf(2);
            }
            break;
        
        case 'deleteCategory':
            $dataCategory = $_POST;
            $id           = $dataCategory['id'];
            if ($category->deleteCategory($id, $dataCategory) == "SUCCESS") {
                printf(1);
            } else {
                printf(2);
            }
            break;
        
        case 'newCategory':
            $dataCategory = $_POST;
            if (($idnuevo = $category->newCategory($dataCategory)) == "ERROR") {
                printf("ERROR");
            } else {
                printf($idnuevo);
            }
            break;
        case 'getDoctors':
            print_r(json_encode($category->getAll()));
            break;
        
        case 'getDoctor':
            $registration = $_GET['registration'];
            if ($registration)
                print_r(json_encode($doctor->getData($registration)));
            break;
        
        case 'existDoctor':
            $registration = $_GET['registration'];
            if ($registration)
                print_r(json_encode($doctor->isExist($registration)));
            break;
        
        case 'editDoctor':
            $dataDoctor = $_POST;
            $registration           = $dataDoctor['registration'];
            if ($doctor->editDoctor($registration, $dataDoctor) == "SUCCESS") {
                printf(1);
            } else {
                printf(2);
            }
            break;
        
        case 'deleteDoctor':
            $dataDoctor = $_POST;
            $registration           = $dataDoctor['registration'];
            if ($doctor->deleteDoctor($registration, $dataDoctor) == "SUCCESS") {
                printf(1);
            } else {
                printf(2);
            }
            break;
        
        case 'newDoctor':
            $dataDoctor = $_POST;
            if (($newregistration = $doctor->newDoctor($dataDoctor)) == "ERROR") {
                printf("ERROR");
            } else {
                printf($newregistration);
            }
            break;

        default:
            # code...
            break;
    }
}

?>
